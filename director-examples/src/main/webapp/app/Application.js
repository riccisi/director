
Ext.define('Director.Application', {

   name: 'Director',

   extend: 'Ext.app.Application',

   views: [
      // TODO: add views here
   ],

   controllers: [
      // TODO: add controllers here
   ],

   stores: [
      // TODO: add stores here
   ],

   launch: function() {

      SimpleAction.sayHello('Simone', function(response, e, success, options) {
         if(success) {
            Ext.log(response);
         }
      });
   }
});
