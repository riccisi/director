package com.director.example.spring;

import com.director.annotation.DirectAction;
import com.director.core.annotation.DirectMethod;
import org.springframework.stereotype.Service;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/11/2014
 */
@Service
@DirectAction
public class SimpleAction {

   @DirectMethod
   public String sayHello(String name) {
      return "Hello " + name + "!";
   }
}
