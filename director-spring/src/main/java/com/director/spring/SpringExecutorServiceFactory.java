package com.director.spring;

import com.director.core.DirectConfiguration;
import com.director.core.ExecutorServiceFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.concurrent.ExecutorService;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/10/2014
 */
public class SpringExecutorServiceFactory implements ExecutorServiceFactory {

   private DirectConfiguration configuration;

   @Required
   public void setConfiguration(DirectConfiguration configuration) {
      this.configuration = configuration;
      this.configuration.registerExecutorServiceFactory(this);
   }

   @Override
   public ExecutorService create(int nThreads) {
      return new ThreadPoolExecutorSpringAware(nThreads);
   }
}
