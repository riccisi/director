package com.director.spring;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/10/2014
 */
public class ThreadPoolExecutorSpringAware extends ThreadPoolExecutor {

   public ThreadPoolExecutorSpringAware(int nThreads) {
      super(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactoryWrapper());
   }

   static class ThreadFactoryWrapper implements ThreadFactory {

      private ThreadFactory defaultFactory = Executors.defaultThreadFactory();

      @Override
      public Thread newThread(Runnable r) {
         RunnableWrapper wrapper = new RunnableWrapper(r);
         wrapper.init();
         return defaultFactory.newThread(wrapper);
      }
   }

   static class RunnableWrapper implements Runnable {

      private Runnable delegate;
      private RequestAttributes requestAttributes;

      RunnableWrapper(Runnable delegate) {
         this.delegate = delegate;
      }

      void init() {
         this.requestAttributes = RequestContextHolder.getRequestAttributes();
      }

      @Override
      public void run() {
         try {
            RequestContextHolder.setRequestAttributes(requestAttributes);
            this.delegate.run();
         } finally {
            RequestContextHolder.resetRequestAttributes();
            this.requestAttributes = null;
         }
      }
   }
}
