package com.director.spring;

import com.director.core.DirectRouter;
import com.director.core.HttpServletInputOutputAdapter;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Simone Ricciardi
 * @version 1.0, 11/07/2010
 */
public class DirectRouterFilter extends OncePerRequestFilter {

   private DirectRouter router;

   @Required
   public void setRouter(DirectRouter router) {
      this.router = router;
   }

   @Override
   protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
      this.router.route(new HttpServletInputOutputAdapter(request, response));
   }
}
