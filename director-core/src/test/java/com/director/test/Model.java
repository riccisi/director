package com.director.test;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/28/2012
 */
public class Model {
   
   private Long id;
   private String prop;

   public Model(Long id, String prop) {
      this.id = id;
      this.prop = prop;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getProp() {
      return prop;
   }

   public void setProp(String prop) {
      this.prop = prop;
   }
}
