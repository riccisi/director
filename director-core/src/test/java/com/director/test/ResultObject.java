package com.director.test;

import java.util.ArrayList;
import java.util.Collection;

/**
* @author Simone Ricciardi
* @version 1.0, 11/01/2011
*/
public class ResultObject {
   private String prop1;
   private int prop2;
   private Collection<ResultObject> multiple = new ArrayList<ResultObject>();
   private ResultObject nested;

   public ResultObject(String prop1, int prop2) {
      this(prop1, prop2, null);
   }

   public ResultObject(String prop1, int prop2, ResultObject nested) {
      this.prop1 = prop1;
      this.prop2 = prop2;
      this.nested = nested;
   }

   public void addMultiple(ResultObject item) {
      this.multiple.add(item);
   }
}
