package com.director.test;

/**
 * @author Simone Ricciardi
 * @version 1.0, 07/10/2011
 */
public class Pojo {

   private Long id;

   public Pojo(Long id) {
      this.id = id;
   }

   public Long getId() {
      return id;
   }

   String print() {
      return "id = " + id;
   }
}
