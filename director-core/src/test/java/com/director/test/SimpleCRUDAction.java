package com.director.test;

import com.director.core.annotation.DirectMethod;
import com.director.core.crud.CRUDAction;
import com.director.core.crud.ReadParameters;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/28/2012
 */
public class SimpleCRUDAction implements CRUDAction<Model> {

   @Override
   @DirectMethod
   public int count(ReadParameters params) {
      return 2;
   }

   @Override
   @DirectMethod
   public Model[] list(ReadParameters params) {
      Model[] result = new Model[0];
      result[0] = new Model(1L, "test1");
      result[1] = new Model(2L, "test2");
      return result;
   }

   @Override
   @DirectMethod
   public Model read(Object id) {
      return new Model(1L, "test");
   }

   @Override
   @DirectMethod
   public Model[] create(Model[] model) {
      model[0].setId(1L);
      return model;
   }

   @Override
   @DirectMethod
   public Model[] update(Model[] model) {
      return model;
   }

   @Override
   @DirectMethod
   public void destroy(Model[] id) {
      // do nothing
   }
}
