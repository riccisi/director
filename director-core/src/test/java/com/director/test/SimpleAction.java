package com.director.test;

import com.director.core.ProviderType;
import com.director.core.annotation.DirectMethod;
import com.director.core.annotation.DirectParam;
import com.director.core.annotation.DirectResult;
import com.director.core.annotation.DirectReturnIncludeStrategy;
import com.director.core.annotation.Pattern;

import java.util.List;
import java.util.Map;

/**
 * @author Simone Ricciardi
 * @version 1.0, 01/09/2011
 */
public class SimpleAction {

   public String nonActionMethod() {
      return "none";   
   }

   @DirectMethod(providerType = ProviderType.POLLING)
   public void ping() {
      System.out.println("Ping executed");
   }

   @DirectMethod
   public String simpleActionMethod() {
      return "test";
   }

   @DirectMethod
   public String methodThatTrowException() {
      throw new RuntimeException("test exception");
   }

   @DirectMethod
   public String methodWithPrimitiveParameters(String param1,
                                               int param2,
                                               long param3,
                                               short param4,
                                               boolean param5,
                                               char param6) {
      return "param1: " + param1 +
             " param2: " + param2 +
             " param3: " + param3 +
             " param4: " + param4 +
             " param5: " + param5 +
             " param6: " + param6;
   }

   @DirectMethod
   public String formHandlingWithMap(Map<String, String> params) {
      String result = "";
      for(String key : params.keySet()) {
         result += key + "," + params.get(key) + ";";
      }
      return result;
   }

   @DirectMethod
   public String formHandlingWithPojo(FormData formData) {
      String result = "param1," + formData.getParam1() + ";";
      result += "param2," + formData.getParam2() + ";";
      return result;
   }

   @DirectMethod
   public String formHandlingWithNamedParameters(
         @DirectParam(name = "param1") String namedParam1,
         @DirectParam(name = "param2") String namedParam2) {
      String result = "param1," + namedParam1 + ";";
      result += "param2," + namedParam2 + ";";
      return result;
   }

   @DirectMethod
   public String methodWithParameterFactory(Pojo pojo) {
      return pojo.print();
   }

   @DirectMethod
   public String methodWithCollectionParameterFactory(List<Pojo> pojoList) {
      String print = "";
      for(Pojo pojo : pojoList) {
         print += pojo.print();
      }
      return print;
   }

   @DirectMethod
   public String methodWithArrayParameterFactory(Pojo[] pojoArray) {
      String print = "";
      for(Pojo pojo : pojoArray) {
         print += pojo.print();
      }
      return print;
   }

   @DirectMethod
   @DirectReturnIncludeStrategy(fieldPatterns = {"prop2", "nested"})
   public ResultObject methodWithReturnIncludeStrategy() {
      ResultObject resultObject = new ResultObject("prop1", 1, new ResultObject("prop2", 2));
      resultObject.addMultiple(new ResultObject("prop3", 3));
      return resultObject;
   }

   @DirectMethod
   public Pojo[] methodWithArray(Pojo[] param) {
      return param;
   }

   @DirectMethod
   @DirectResult( includes = @Pattern(type = Pojo.class, fields = {"","",""}) )
   public Pojo[] method(Pojo[] param) {
      return param;
   }
}
