package com.director.core;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintStream;
import java.io.Writer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


/**
 * @author Simone Ricciardi
 * @version 1.0, 03/06/2011
 */
public class DirectPollingProviderTest extends AbstractDirectTest {

   @Test
   public void testExecutePingMethod() throws Exception {

      final PrintStream mockedOut = mock(PrintStream.class);
      System.setOut(mockedOut);

      doRequest(new RequestWorker() {
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider(pollingProviderId);
            provider.process(new HttpServletInputOutputAdapter(request, response));
            verify(mockedOut).println("Ping executed");
         }
      });
   }
}
