package com.director.core;

import com.director.test.SimpleAction;
import com.director.test.SimpleCRUDAction;
import org.junit.Before;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Simone Ricciardi
 * @version 1.0, 03/05/2011
 */
public abstract class AbstractDirectTest {

   protected String pollingProviderId;
   DirectConfiguration configuration;

   @Before
   public void setUp() throws Exception {
      this.configuration = new DirectConfiguration();
      this.registerClass();
   }

   protected void doRequest(final RequestWorker requestWorker) throws Exception {
      HttpServletRequest request = mock(HttpServletRequest.class);
      HttpServletResponse response = mock(HttpServletResponse.class);
      Writer out = new StringWriter();
      PrintWriter writer = new PrintWriter(out);
      when(response.getWriter()).thenReturn(writer);
      requestWorker.doInRequest(request, response, out);
   }

   protected void doJsonRequest(final String action, final String method, final RequestWorker requestWorker, final Object... data) throws Exception {
      this.doRequest(new RequestWorker() {
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Transaction transaction = new Transaction(1, action, method, data);
            String directTransaction = toJson(transaction);
            BufferedReader reader = new BufferedReader(new StringReader(directTransaction));
            when(request.getReader()).thenReturn(reader);
            requestWorker.doInRequest(request, response, out);
         }
      });
   }

   protected void startFormRequest(final String action, final String method, final Map<String, String> data, final RequestWorker requestWorker) throws Exception {
      this.doRequest(new RequestWorker() {
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            when(request.getContentType()).thenReturn("application/x-www-form-urlencoded");
            when(request.getMethod()).thenReturn("post");

            Vector<String> params = new Vector<String>();
            params.add("extTID");
            params.add("extAction");
            params.add("extMethod");
            params.add("extType");
            for(String key : data.keySet()) {
               params.add(key);
            }
            Enumeration paramNames = params.elements();

            when(request.getParameterNames()).thenReturn(paramNames);
            when(request.getParameter("extTID")).thenReturn("1");
            when(request.getParameter("extAction")).thenReturn(action);
            when(request.getParameter("extMethod")).thenReturn(method);
            when(request.getParameter("extType")).thenReturn("rpc");
            for(Map.Entry<String, String> entry : data.entrySet()) {
               when(request.getParameter(entry.getKey())).thenReturn(entry.getValue());
            }
            requestWorker.doInRequest(request, response, out);
         }
      });
   }

   protected String toJson(Object object) {
      return this.configuration.getParser().format(object);
   }

   protected <T> T fromJson(String json, Class<T> aClass) {
      return this.configuration.getParser().parse(json, aClass);
   }

   private void registerClass() throws Exception {
      this.configuration.registerClass("com.director.test.SimpleAction", "test", "testAction");
      this.configuration.registerClass("com.director.test.SimpleCRUDAction", "test", "testCRUDAction");
      Collection<Provider> providers = this.configuration.getProviders();
      for(Provider provider : providers) {
         if(provider instanceof RemotingProvider) {
            assertEquals(provider.getId(), "test-remoting-provider");
            assertEquals(provider.getNamespace(), "test");
            DirectAction[] actions = ((RemotingProvider) provider).getActions();
            assertEquals(actions.length, 2);
            DirectAction action = actions[0];
            if("testAction".equals(action.getName())) {
               assertEquals(SimpleAction.class, action.getActionClass());
               assertEquals(9, action.getMethods().length);
            }
            if("testCRUDAction".equals(action.getName())) {
               assertEquals(SimpleCRUDAction.class, action.getActionClass());
               assertEquals(6, action.getMethods().length);
            }
         }
         if(provider instanceof PollingProvider) {
            assertTrue(provider.getId().startsWith("test-polling-provider"));
            assertEquals(provider.getNamespace(), "test");
            PollingProvider pollingProvider = ((PollingProvider) provider);
            pollingProviderId = pollingProvider.getId();
            assertEquals(pollingProvider.getInterval(), 3000);
            DirectAction action = pollingProvider.getDirectAction();
            DirectMethod actionMethod = pollingProvider.getDirectMethod();
            assertEquals("testAction", action.getName());
            assertEquals(SimpleAction.class, action.getActionClass());
            assertEquals("ping", actionMethod.getName());
         }
      }
   }

   static class Transaction {
      int tid;
      String type = "rpc";
      String action;
      String method;
      Object[] data;

      Transaction(int tid, String action, String method, Object[] data) {
         this.tid = tid;
         this.action = action;
         this.method = method;
         this.data = data;
      }

      public int getTid() {
         return tid;
      }

      public String getType() {
         return type;
      }

      public String getAction() {
         return action;
      }

      public String getMethod() {
         return method;
      }

      public Object getData() {
         return data;
      }
   }

   interface RequestWorker {

      void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception;
   }
}
