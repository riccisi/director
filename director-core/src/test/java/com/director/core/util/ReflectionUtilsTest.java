package com.director.core.util;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Simone Ricciardi
 * @version 1.0, 04/07/2012
 */
public class ReflectionUtilsTest {
   
   @Test
   public void testHierarchycalGetDeclaredField() throws Exception {
      try {
         B.class.getDeclaredField("field1");
         fail();
      } catch(NoSuchFieldException e) {
      } 
      Field field = ReflectionUtils.hierarchicalGetDeclaredField(B.class, "field1");
      assertEquals("field1", field.getName());
   }

   @Test
   public void testHierarchycalGetDeclaredFields() throws Exception {
      assertEquals(1, B.class.getDeclaredFields().length);
      assertEquals(2, ReflectionUtils.hierarchicalGetDeclaredFields(B.class).size());
   }
   
   static class A {
      private int field1;   
   }
   
   static class B extends A {
      private int field2;   
   }
}
