package com.director.core;

import com.director.test.Model;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Writer;

import static org.junit.Assert.assertEquals;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/28/2012
 */
public class CRUDActionTest extends AbstractDirectTest {

   @Test
   public void testCreate() throws Exception {
      this.doJsonRequest("testCRUDAction", "create", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testCRUDAction\",\"method\":\"create\",\"result\":{\"success\":true,\"data\":[{\"id\":1,\"prop\":\"test\"}]},\"type\":\"rpc\"}]", result);
            System.out.println("result = " + result);
         }
      }, new Model(null, "test"));
   }

   @Test
   public void testRead() {

   }

   @Test
   public void testUpdate() throws Exception {
      this.doJsonRequest("testCRUDAction", "update", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testCRUDAction\",\"method\":\"update\",\"result\":{\"success\":true,\"data\":[{\"id\":1,\"prop\":\"test\"}]},\"type\":\"rpc\"}]", result);
            System.out.println("result = " + result);
         }
      }, new Model(1L, "test"));
   }

   @Test
   public void testDelete() throws Exception {
      this.doJsonRequest("testCRUDAction", "destroy", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testCRUDAction\",\"method\":\"destroy\",\"result\":{\"success\":true},\"type\":\"rpc\"}]", result);
            System.out.println("result = " + result);
         }
      }, new Model(null, "test"));
   }
}
