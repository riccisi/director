package com.director.core;

import com.director.test.Pojo;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Simone Ricciardi
 * @version 1.0, 01/30/2011
 */
public class DirectRemotingProviderTest extends AbstractDirectTest {

   @Test
   public void testExecuteSimpleMethod() throws Exception {
      this.doJsonRequest("testAction", "simpleActionMethod", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testAction\",\"method\":\"simpleActionMethod\",\"result\":\"test\",\"type\":\"rpc\"}]", result);
         }
      }, null);
   }

   @Test
   public void testExecuteMethodThatThrowException() throws Exception {
      this.doJsonRequest("testAction", "methodThatTrowException", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            DirectExceptionEvent[] exceptions = fromJson(result, DirectExceptionEvent[].class);
            assertEquals(1, exceptions.length);
            assertEquals("exception", exceptions[0].getType());
            assertNotNull(exceptions[0].getWhere());
         }
      }, null);
   }

   @Test
   public void testExecuteMethodWithPrimitiveParameters() throws Exception {
      this.doJsonRequest("testAction", "methodWithPrimitiveParameters", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testAction\",\"method\":\"methodWithPrimitiveParameters\",\"result\":\"param1: 1 param2: 1 param3: 1 param4: 1 param5: true param6: 1\",\"type\":\"rpc\"}]", result);
         }
      }, "1", 1, 1L, ((short) 1), true, '1');
   }

   @Test
   public void testExecuteFormMethod() throws Exception {

      Map<String, String> formData = new HashMap<String, String>();
      formData.put("param1", "Value1");
      formData.put("param2", "Value2");
      formData.put("param3", "Value/3");

      this.startFormRequest("testAction", "formHandlingWithMap", formData, new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();

            assertEquals("<html><body><textarea>[" +
                  "{\"tid\":1,\"action\":\"testAction\",\"method\":\"formHandlingWithMap\",\"result\":\"param1,Value1;param2,Value2;param3,Value/3;\",\"type\":\"rpc\"" +
                  "}]</textarea></body></html>",result);
         }
      });

      this.startFormRequest("testAction", "formHandlingWithPojo", formData, new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();

            assertEquals("<html><body><textarea>[" +
               "{\"tid\":1,\"action\":\"testAction\",\"method\":\"formHandlingWithPojo\",\"result\":\"param1,Value1;param2,Value2;\",\"type\":\"rpc\"" +
               "}]</textarea></body></html>", result);
         }
      });

      this.startFormRequest("testAction", "formHandlingWithNamedParameters", formData, new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();

            assertEquals("<html><body><textarea>[" +
               "{\"tid\":1,\"action\":\"testAction\",\"method\":\"formHandlingWithNamedParameters\",\"result\":\"param1,Value1;param2,Value2;\",\"type\":\"rpc\"" +
               "}]</textarea></body></html>", result);
         }
      });
   }

   @Test
   public void testParameterFactory() throws Exception {

      configuration.registerParameterFactory(Pojo.class, new PojoParameterFactory());

      this.doJsonRequest("testAction", "methodWithParameterFactory", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testAction\",\"method\":\"methodWithParameterFactory\",\"result\":\"id \\u003d 1\",\"type\":\"rpc\"}]", result);
         }
      }, 1L);

      this.doJsonRequest("testAction", "methodWithCollectionParameterFactory", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testAction\",\"method\":\"methodWithCollectionParameterFactory\",\"result\":\"id \\u003d 1\",\"type\":\"rpc\"}]", result);
         }
      }, new Object[]{new Long[]{1L}});

      this.doJsonRequest("testAction", "methodWithArrayParameterFactory", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            assertEquals("[{\"tid\":1,\"action\":\"testAction\",\"method\":\"methodWithArrayParameterFactory\",\"result\":\"id \\u003d 1\",\"type\":\"rpc\"}]", result);
         }
      }, new Object[]{new Long[]{1L}});
   }

   @Test
   public void testMethodWithReturnIncludeStrategy() throws Exception {

      this.doJsonRequest("testAction", "methodWithReturnIncludeStrategy", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            System.out.println("result = " + result);
         }
      });
   }

   @Test
   public void testMethodWithArray() throws Exception {

      this.doJsonRequest("testAction", "methodWithArray", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            System.out.println("result = " + result);
         }
      }, new Object[]{new Pojo[]{new Pojo(1L), new Pojo(2L)}});

      this.doJsonRequest("testAction", "methodWithArray", new RequestWorker() {
         @Override
         public void doInRequest(HttpServletRequest request, HttpServletResponse response, Writer out) throws Exception {
            Provider provider = configuration.getProvider("test-remoting-provider");
            provider.process(new HttpServletInputOutputAdapter(request, response));
            String result = out.toString();
            System.out.println("result = " + result);
         }
      }, new Pojo(1L));
   }

   class PojoParameterFactory implements ParameterFactory<Long,Pojo> {

      public Type getInputType() {
         return Long.class;
      }

      public Pojo create(Type type, Long input) {
         return new Pojo(input);
      }
   }

}
