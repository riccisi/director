package com.director.core;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 13.28.50
 */
public class DirectTransaction extends DirectEvent {

   private int tid;
   private String action = "";
   private String method = "";
   private DirectTransactionData data;
   private DirectTransactionResult result;

   public void setTid(int tid) {
      this.tid = tid;
   }

   public void setAction(String action) {
      this.action = action;
   }

   public void setMethod(String method) {
      this.method = method;
   }

   public void setData(DirectTransactionData data) {
      this.data = data;
   }

   public int getTid() {
      return tid;
   }

   public String getAction() {
      return action;
   }

   public String getMethod() {
      return method;
   }

   public DirectTransactionResult getResult() {
      return result;
   }

   /**
    * Execute the current transaction invoking the configured action method.
    *
    * @param provider provider
    * @throws DirectException
    */
   public void execute(RemotingProvider provider) throws DirectException {
      if(!provider.containsAction(this.action)) {
         throw new DirectException("No action class found with name '" + this.action + "'");
      }
      DirectAction directAction = provider.getAction(this.action);
      this.result = directAction.invokeMethod(this.method, this.data);
   }
}
