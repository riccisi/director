package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/31/2011
 */
class ProviderFactory {

   static Provider create(String id, String namespace, ProviderType type, DirectConfiguration configuration) {
      switch(type) {
         case POLLING:
            return new PollingProvider(id, namespace, type, configuration);
         case REMOTING:
            return new RemotingProvider(id, namespace, type, configuration);
         default:
            throw new IllegalArgumentException("No concrete provider class found for type: " + type);
      }
   }
}
