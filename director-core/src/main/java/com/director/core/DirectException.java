package com.director.core;

/**
 * Author: Simone Ricciardi
 * Date: 31-mag-2010
 * Time: 16.34.50
 */
public class DirectException extends RuntimeException {

   public DirectException() {
   }

   public DirectException(String message) {
      super(message);
   }

   public DirectException(String message, Throwable cause) {
      super(message, cause);
   }

   public DirectException(Throwable cause) {
      super(cause);
   }
}
