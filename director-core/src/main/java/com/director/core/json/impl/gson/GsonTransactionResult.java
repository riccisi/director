package com.director.core.json.impl.gson;

import com.director.core.DirectMethod;
import com.director.core.DirectTransactionResult;
import com.director.core.annotation.DirectResult;
import com.director.core.annotation.Pattern;
import com.director.core.annotation.Serializer;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

/**
 * @author Simone Ricciardi
 * @version 1.0, 11/29/2011
 */
public class GsonTransactionResult implements DirectTransactionResult {

   private JsonElement jsonResult;

    private Object result;

   public GsonTransactionResult(final GsonBuilder gsonBuilder, DirectMethod directMethod, Object result) {
/*
      Object resultToParse = result;
      if(directMethod.hasReturnDataStrategy()) {
         resultToParse = new ResultWithStrategy(directMethod.getReturnDataStrategy(), result);
      }

*/
      this.result = result;

      try {
         if(directMethod.hasResultAnnotation()) {
            DirectResult directResult = directMethod.getResultAnnotation();
            for(Pattern pattern : directResult.includes()) {
               gsonBuilder.registerTypeAdapter(pattern.type(), new IncludeFieldsTypeAdapter(pattern.fields()));
            }
            for(Pattern pattern : directResult.excludes()) {
               gsonBuilder.registerTypeAdapter(pattern.type(), new ExcludeFieldsTypeAdapter(pattern.fields()));
            }
            for(Serializer serializer : directResult.serializers()) {
               if(serializer.hierarchy()) {
                  gsonBuilder.registerTypeHierarchyAdapter(serializer.type(), serializer.impl().newInstance());
               } else {
                  gsonBuilder.registerTypeAdapter(serializer.type(), serializer.impl().newInstance());
               }
            }
         }
      } catch(Exception e) {
         throw new RuntimeException(e);
      }

      this.jsonResult = gsonBuilder.create().toJsonTree(result);
   }

   public JsonElement getJsonResult() {
      return jsonResult;
   }

    @Override
    public Object getRawResult() {
        return result;
    }
}
