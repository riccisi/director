package com.director.core.json;

import com.director.core.DirectMethod;
import com.director.core.DirectTransactionResult;

import java.lang.reflect.Type;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 11.52.48
 */
public interface JsonParser {

   <T> T parse(String json, Class<T> type);

   Object parse(String json, Type type);

   String format(Object object);

   void format(Object object, Appendable appendable);

   DirectTransactionResult buildResult(DirectMethod directMethod, Object result);
}
