package com.director.core.json.impl.gson;

import com.google.gson.GsonBuilder;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/15/2011
 */
public interface GsonBuilderConfigurer {

   void config(GsonBuilder builder);
}
