package com.director.core.json.impl.gson;

import com.director.core.DirectTransactionData;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/28/2011
 */
public class TransactionDataExclusionStrategy implements ExclusionStrategy{

   @Override
   public boolean shouldSkipField(FieldAttributes f) {
      return false;
   }

   @Override
   public boolean shouldSkipClass(Class<?> clazz) {
      return DirectTransactionData.class.equals(clazz);
   }
}
