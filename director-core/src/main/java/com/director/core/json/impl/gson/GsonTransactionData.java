package com.director.core.json.impl.gson;

import com.director.core.DirectException;
import com.director.core.DirectTransactionData;
import com.director.core.ParameterFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Simone Ricciardi
 * Date: 1-giu-2010
 * Time: 11.24.03
 */
public class GsonTransactionData implements DirectTransactionData {

    private JsonElement json;
    private JsonDeserializationContext context;

    public GsonTransactionData(JsonElement json, JsonDeserializationContext context) {
        this.json = json;
        this.context = context;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T parseValue(int order, Type type) {
        if (json.isJsonArray()) {
            JsonArray jsonArray = json.getAsJsonArray();
            if (jsonArray.size() <= order) {
                throw new DirectException("Transaction data list of values cannot doesn't contain parameter with order index: " + order + " cause is too short: " + jsonArray.size());
            }
            JsonElement jsonElement = jsonArray.get(order);

            if (type instanceof ParameterizedType && Collection.class.isAssignableFrom((Class) ((ParameterizedType) type).getRawType()) && jsonElement.isJsonObject()) {
                Class collectionClass = (Class) ((ParameterizedType) type).getRawType();
                Object collectionArg = this.context.deserialize(jsonElement, this.getComponentType(type));
                Collection collection = Set.class.isAssignableFrom(collectionClass) ? new HashSet(1) : new ArrayList(1);
                collection.add(collectionArg);
                return (T) collection;
            }

            if (this.isArray(type) && jsonElement.isJsonObject()) {
                Object arrayArg = this.context.deserialize(jsonElement, this.getArrayComponentType(type));
                Object[] array = (Object[]) Array.newInstance(this.getArrayComponentType(type), 1);
                array[0] = arrayArg;
                return (T) array;
            }

            return (T) this.context.deserialize(jsonElement, type);
        }
        throw new DirectException("Transaction data aren't a list of values, cannot access through parameter order index: " + order);
    }

    private Class getComponentType(Type type) {
        if(type instanceof Class) {
            return ((Class) type).getComponentType();
        }
        if(type instanceof ParameterizedType) {
            return ((Class)((ParameterizedType) type).getActualTypeArguments()[0]);
        }
        throw new IllegalArgumentException("type " + type + " is not an array type");
    }

    private Class getArrayComponentType(Type type) {
        if (type instanceof Class && ((Class) type).isArray()) {
            return ((Class) type).getComponentType();
        }
        if (type instanceof GenericArrayType) {
            return (Class) ((GenericArrayType) type).getGenericComponentType();
        }
        throw new IllegalArgumentException("type " + type + " is not an array type");
    }

    private boolean isArray(Type type) {
        return (type instanceof Class && ((Class) type).isArray()) || type instanceof GenericArrayType;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T parseValue(String name, Type type) {
        JsonElement jsonData = json;
        if (json.isJsonArray()) {
            jsonData = ((JsonArray) json).get(0);
        }
        if (jsonData.isJsonObject()) {
            JsonObject jsonObject = jsonData.getAsJsonObject();
            JsonElement jsonElement = jsonObject.get(name);
            return (T) this.context.deserialize(jsonElement, type);
        }
        throw new DirectException("Transaction data aren't a list of names/values pairs, cannot access through parameter name: " + name);
    }
}
