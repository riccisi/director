package com.director.core.json.impl.gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * @author Simone Ricciardi
 * @version 1.0, 11/29/2011
 */
public class TransactionResultTypeAdapter implements JsonSerializer<GsonTransactionResult> {

   @Override
   public JsonElement serialize(GsonTransactionResult src, Type typeOfSrc, JsonSerializationContext context) {
      return src.getJsonResult();
   }
}
