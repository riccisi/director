package com.director.core.json.impl.gson;

import com.director.core.PollingProvider;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/31/2011
 */
public class PollingProviderTypeAdapter implements JsonSerializer<PollingProvider> {

   @Override
   public JsonElement serialize(PollingProvider src, Type typeOfSrc, JsonSerializationContext context) {
      JsonObject jsonProvider = new JsonObject();
      jsonProvider.addProperty("id", src.getId());
      jsonProvider.addProperty("url", src.getUrl());
      jsonProvider.addProperty("type", src.getType());
      jsonProvider.addProperty("namespace", src.getNamespace());
      jsonProvider.addProperty("interval", src.getInterval());
      return jsonProvider;
   }
}
