package com.director.core.json.impl.gson;

import com.director.core.DirectTransactionData;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Author: Simone Ricciardi
 * Date: 4-giu-2010
 * Time: 17.51.28
 */
public class TransactionDataTypeAdapter implements JsonDeserializer<DirectTransactionData> {

   public DirectTransactionData deserialize(JsonElement json,
                                            Type typeOfT,
                                            JsonDeserializationContext context) throws JsonParseException {
      return new GsonTransactionData(json, context);
   }
}
