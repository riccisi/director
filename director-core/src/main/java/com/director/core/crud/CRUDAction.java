package com.director.core.crud;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/27/2012
 */
public interface CRUDAction<T> {

   int count(ReadParameters params);
   
   T[] list(ReadParameters params);
   
   T read(Object id);

   T[] create(T[] model);

   T[] update(T[] model);

   void destroy(T[] id);
}
