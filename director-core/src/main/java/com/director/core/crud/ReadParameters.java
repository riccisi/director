package com.director.core.crud;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/27/2012
 */
public class ReadParameters {
   
   private int NO_VALUE = -1;
   
   private Filter[] filters = new Filter[0];
   private Sorter[] sorters = new Sorter[0];
   private int limit = NO_VALUE;
   private int start = NO_VALUE;
   private int total = NO_VALUE;

   public Filter[] getFilters() {
      return filters;
   }
   
   public boolean hasFilters() {
      return this.filters.length > 0;
   }

   public Sorter[] getSorters() {
      return sorters;
   }

   public boolean hasSorters() {
      return this.sorters.length > 0;   
   }
   
   public int getLimit() {
      return limit;
   }

   public int getStart() {
      return start;
   }

   public boolean requirePagination() {
      return  limit != NO_VALUE && start != NO_VALUE;
   }

   public void setTotal(int total) {
      this.total = total;
   }

   public int getTotal() {
      return total;
   }

   public boolean hasTotal() {
      return total != NO_VALUE;
   }
}
