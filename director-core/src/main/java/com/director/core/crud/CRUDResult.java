package com.director.core.crud;

import com.director.core.DirectTransactionResult;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/27/2012
 */
public class CRUDResult implements DirectTransactionResult {
   
   private boolean success;
   private String message;
   private DirectTransactionResult data;
   private Long total;

   public CRUDResult(boolean success, String message, DirectTransactionResult data) {
      this.success = success;
      this.message = message;
      this.data = data;
   }

   public boolean isSuccess() {
      return success;
   }

   public String getMessage() {
      return message;
   }

   public DirectTransactionResult getData() {
      return data;
   }

   @Override
   public Object getRawResult() {
      return null;
   }
}
