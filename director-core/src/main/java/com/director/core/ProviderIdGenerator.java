package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 01/09/2011
 */
public interface ProviderIdGenerator {
   String generateId(String namespace);
}
