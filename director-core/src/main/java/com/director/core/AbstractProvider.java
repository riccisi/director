package com.director.core;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Author: Simone Ricciardi
 * Date: 8-giu-2010
 * Time: 11.53.47
 */
public abstract class AbstractProvider implements Provider {

   private static final Log LOG = LogFactory.getLog(AbstractProvider.class);

   private String id;
   private String namespace;
   private ProviderType type;
   private DirectConfiguration configuration;

   protected AbstractProvider(String id, String namespace, ProviderType type, DirectConfiguration configuration) {
      this.id = id;
      this.namespace = namespace;
      this.type = type;
      this.configuration = configuration;
   }

   public String getUrl() {
      String ns = StringUtils.isBlank(this.namespace) ? "" : this.namespace +"/";
      String routerUrl = this.configuration.getRouterUrl();
      String providerParamName = this.configuration.getProviderParamName();
      return String.format("%s%s?%s=%s", ns, routerUrl, providerParamName, this.id);
   }

   public String getId() {
      return this.id;
   }

   public String getNamespace() {
      return this.namespace;
   }

   public String getType() {
      return this.type.getTypeName();
   }

   public void process(InputOutputAdapter inputOutputAdapter) {
      try {
         DirectContext.init(inputOutputAdapter, this.configuration);
         this.handleProcess(inputOutputAdapter);
         this.handleResponse(inputOutputAdapter);
      } finally {
         DirectContext.dispose();
      }
   }

   private void handleProcess(InputOutputAdapter inputOutputAdapter) {
      try {
         this.doProcess(inputOutputAdapter);
      } catch(Throwable e) {
         LOG.error("Provider error, provider id " + this.id + " error executing direct request ", e);
         DirectContext.get().pushEvent(new DirectExceptionEvent(e));
      }
   }

   private void handleResponse(InputOutputAdapter inputOutputAdapter) {
      try {
         inputOutputAdapter.write(DirectContext.get().getEvents());
      } catch(Exception e) {
         LOG.error("Provider error, provider id " + this.id + " error formatting direct request output", e);
      }
   }

   protected abstract void doProcess(InputOutputAdapter inputOutputAdapter) throws Throwable;
}
