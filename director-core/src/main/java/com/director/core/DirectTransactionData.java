package com.director.core;

import java.lang.reflect.Type;

/**
 * Author: Simone Ricciardi
 * Date: 6-giu-2010
 * Time: 11.29.30
 */
public interface DirectTransactionData {

   /**
    * Parse the transaction data parameter acceding by order.
    * (The data are provided as a list of ordered parameter values)
    *
    * @param order
    * @param type
    * @return
    */
   <T> T parseValue(int order, Type type);

   /**
    * Parse the transaction data parameter acceding by parameter name.
    * (The data are provided as a list of named/values pairs, like a Map)
    *
    * @param name
    * @param type
    * @return
    */
   <T> T parseValue(String name, Type type);
}
