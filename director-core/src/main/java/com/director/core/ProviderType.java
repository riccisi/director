package com.director.core;

/**
 * Author: Simone Ricciardi
 * Date: 8-ago-2010
 * Time: 12.16.06
 */
public enum ProviderType {

   REMOTING("remoting", new RemotingProviderIdGenerator()),
   POLLING("polling", new PollingProviderIdGenerator());

   private ProviderIdGenerator idGenerator;
   private String typeName;

   private ProviderType(String typeName, ProviderIdGenerator idGenerator) {
      this.typeName = typeName;
      this.idGenerator = idGenerator;
   }

   String getTypeName() {
      return typeName;
   }

   String getProviderId(String namespace) {
      return idGenerator.generateId(namespace);
   }
}
