package com.director.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Simone Ricciardi
 * @version 1.0, 04/07/2012
 */
public class ReflectionUtils {

   public static Field hierarchicalGetDeclaredField(Class src, String fieldName) throws NoSuchFieldException {
      try {
         return src.getDeclaredField(fieldName);
      } catch(NoSuchFieldException e) {
         Class father = src.getSuperclass();
         if(father != null) {
            return hierarchicalGetDeclaredField(father, fieldName);
         } else {
            throw e;
         }
      }
   }

   public static List<Field> hierarchicalGetDeclaredFields(Class src) {
      List<Field> fields = new ArrayList<Field>();
      Class father = src.getSuperclass();
      if(father != null) {
         fields.addAll(hierarchicalGetDeclaredFields(father));
      }
      fields.addAll(Arrays.asList(src.getDeclaredFields()));
      return fields;
   }
}
