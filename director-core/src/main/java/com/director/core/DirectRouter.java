package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/10/2013
 */
public class DirectRouter {

   private final DirectConfiguration configuration;

   public DirectRouter(DirectConfiguration configuration) {
      this.configuration = configuration;
   }

   public void route(InputOutputAdapter inputOutputAdapter) {
      String providerId = inputOutputAdapter.readProviderId();
      if(providerId != null) {
         Provider provider = this.configuration.getProvider(providerId);
         provider.process(inputOutputAdapter);
      } else {
         String api = this.configuration.getFormattedApi();
         inputOutputAdapter.write(api);
      }
   }
}
