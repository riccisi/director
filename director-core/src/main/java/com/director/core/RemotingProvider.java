package com.director.core;

import com.director.core.crud.CRUDAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 12.00.06
 */
public class RemotingProvider extends AbstractProvider {

   private static final Log LOG = LogFactory.getLog(RemotingProvider.class);

   private Map<String, DirectAction> actions = new HashMap<String, DirectAction>();

   protected RemotingProvider(String id, String namespace, ProviderType type, DirectConfiguration configuration) {
      super(id, namespace, type, configuration);
   }

   @Override
   public void registerMethod(String actionName, String methodName, Class actionClass, Method method) {
      DirectAction directAction = this.actions.get(actionName);
      if(directAction == null) {
         if(CRUDAction.class.isAssignableFrom(actionClass)) {
            directAction = new DirectCRUDAction(this.getNamespace(), actionName, actionClass);
         } else {
            directAction = new DirectAction(this.getNamespace(), actionName, actionClass);
         }
         this.actions.put(actionName, directAction);
      }
      directAction.addMethod(methodName, method);
   }

   @Override
   public void doProcess(InputOutputAdapter inputOutputAdapter) throws Throwable {
      DirectTransaction[] transactions = inputOutputAdapter.readTransactions();
      if(transactions == null) {
         LOG.info("Remoting provider called without transaction in input...");
         return;
      }
      final DirectContext directContext = DirectContext.get();
      ExecutorService executor = directContext.getConfiguration().createExecutorService(transactions.length);
      for(final DirectTransaction transaction : transactions) {
         executor.execute(new Runnable() {
            public void run() {
               try {
                  DirectContext.set(directContext);
                  transaction.execute(RemotingProvider.this);
                  DirectContext.get().pushEvent(transaction);
               } catch(Throwable e) {
                  LOG.error("Provider error, provider id " + getId() + " error executing direct request ", e);
                  DirectContext.get().pushEvent(new DirectExceptionEvent(transaction.getTid(), e));
               } finally {
                  DirectContext.dispose();
               }
            }
         });
      }
      executor.shutdown();
      while (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
      }
   }

   public DirectAction[] getActions() {
      return this.actions.values().toArray(new DirectAction[this.actions.size()]);
   }

   public boolean containsAction(String name) {
      return this.actions.containsKey(name);
   }

   public DirectAction getAction(String name) {
      return this.actions.get(name);
   }
}
