package com.director.core;

import org.apache.commons.lang.StringUtils;

/**
 * @author Simone Ricciardi
 * @version 1.0, 01/09/2011
 */
public class PollingProviderIdGenerator implements ProviderIdGenerator{

   private int sequence = 0;

   public String generateId(String namespace) {
      String ns = StringUtils.isBlank(namespace) ? "" : namespace+"-";
      return String.format("%spolling-provider-%s", ns, ++sequence);
   }
}
