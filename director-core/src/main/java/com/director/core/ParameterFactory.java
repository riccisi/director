package com.director.core;

import java.lang.reflect.Type;

/**
 * @author Simone Ricciardi
 * @version 1.0, 07/09/2011
 */
public interface ParameterFactory<T,E> {

   Type getInputType();

   E create(Type type, T input);
}
