package com.director.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A direct context bind to a single request thread direct invocation.
 *
 * Author: Simone Ricciardi
 * Date: 2-lug-2010
 * Time: 14.34.42
 */
public class DirectContext {

   private static ThreadLocal<DirectContext> CONTEXT = new ThreadLocal<DirectContext>();

   private DirectConfiguration configuration;
   private InputOutputAdapter inputOutputAdapter;
   private List<DirectEvent> events = Collections.synchronizedList(new ArrayList<DirectEvent>());

   private DirectContext(InputOutputAdapter inputOutputAdapter, DirectConfiguration configuration) {
      this.inputOutputAdapter = inputOutputAdapter;
      this.configuration = configuration;
   }

   /**
    * Returns the DirectRequestContext specific to the current thread.
    *
    * @return the DirectRequestContext for the current thread, is never <tt>null</tt>.
    */
   public static DirectContext get() {
      return CONTEXT.get();
   }

   /**
    *
    * @param inputOutputAdapter
    * @param configuration
    *
    */
   static void init(InputOutputAdapter inputOutputAdapter, DirectConfiguration configuration) {
      set(new DirectContext(inputOutputAdapter, configuration));
   }

   /**
    *
    * @param context
    */
   static void set(DirectContext context) {
      CONTEXT.set(context);
   }

   /**
    *
    */
   static void dispose() {
      CONTEXT.set(null);
   }

   public InputOutputAdapter getInputOutputAdapter() {
      return inputOutputAdapter;
   }

   /**
    *
    * @return
    */
   public DirectConfiguration getConfiguration() {
      return configuration;
   }

   /**
    *
    * @return
    */
   public DirectEvent[] getEvents() {
      return events.toArray(new DirectEvent[events.size()]);
   }

   /**
    *
    * @param event
    */
   public void pushEvent(DirectEvent event) {
      this.events.add(event);
   }

}
