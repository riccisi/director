package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 11/28/2011
 */
public class ResultWithStrategy {

   private ReturnDataStrategy strategy;
   private Object result;

   public ResultWithStrategy(ReturnDataStrategy strategy, Object result) {
      this.strategy = strategy;
      this.result = result;
   }

   public ReturnDataStrategy getStrategy() {
      return strategy;
   }

   public Object getResult() {
      return result;
   }
}
