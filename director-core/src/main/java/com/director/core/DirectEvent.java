package com.director.core;

import java.io.Serializable;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 13.13.04
 */
public class DirectEvent implements Serializable {

   public static final String DEFAULT_EVENT_TYPE = "event";
   
   protected String type = DEFAULT_EVENT_TYPE;

   public void setType(String type) {
      this.type = type;
   }

   public String getType() {
      return type;
   }

}
