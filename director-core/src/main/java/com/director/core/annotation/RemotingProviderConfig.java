package com.director.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 11.15.54
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RemotingProviderConfig {
   boolean formHandler() default false;
}
