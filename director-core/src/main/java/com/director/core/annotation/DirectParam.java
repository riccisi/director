package com.director.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Simone Ricciardi
 * @version 1.0, 07/09/2011
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface DirectParam {
   String name() default "";
   String defaultValue() default "";
}
