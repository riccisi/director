package com.director.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author: Simone Ricciardi
 * Date: 8-giu-2010
 * Time: 12.11.53
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PollingProviderConfig {
   long interval() default 3000;
}
