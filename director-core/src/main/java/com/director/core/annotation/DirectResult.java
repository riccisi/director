package com.director.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Simone Ricciardi
 * @version 1.0, 04/07/2012
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DirectResult {
   Pattern[] includes() default {};
   Pattern[] excludes() default {};
   Serializer[] serializers() default {};
}
