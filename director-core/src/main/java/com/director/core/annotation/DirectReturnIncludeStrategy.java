package com.director.core.annotation;

import com.director.core.IncludeReturnDataStrategy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/28/2011
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@DirectReturn(strategy = IncludeReturnDataStrategy.class)
public @interface DirectReturnIncludeStrategy {
   String[] fieldPatterns() default {};
}
