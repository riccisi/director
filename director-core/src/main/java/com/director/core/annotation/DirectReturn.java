package com.director.core.annotation;

import com.director.core.ReturnDataStrategy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/22/2011
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DirectReturn {
   Class<? extends ReturnDataStrategy> strategy();
}
