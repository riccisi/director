package com.director.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Simone Ricciardi
 * @version 1.0, 04/07/2012
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface Pattern {
   Class type();
   String[] fields();
}
