package com.director.core.annotation;

import com.director.core.ProviderType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author: Simone Ricciardi
 * Date: 8-ago-2010
 * Time: 12.33.06
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DirectMethod {
   String name() default "";
   ProviderType providerType() default ProviderType.REMOTING;
}
