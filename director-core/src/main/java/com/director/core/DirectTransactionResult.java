package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 11/28/2011
 */
public interface DirectTransactionResult {

    Object getRawResult();

}
