package com.director.core;

import java.util.List;
import java.util.Map;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/25/2011
 */
public interface ReturnDataStrategy {
   Map<Object,List<String>> buildSerializationMap(Object result);
}
