package com.director.core;

/**
 * Author: Simone Ricciardi
 * Date: 2-lug-2010
 * Time: 12.10.32
 */
public interface ExecutorAdapter {

   DirectTransactionResult execute(DirectAction directAction, DirectMethod directMethod, DirectTransactionData data) throws Throwable;
}
