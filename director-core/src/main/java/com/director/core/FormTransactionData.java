package com.director.core;

import com.director.core.json.JsonParser;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Author: Simone Ricciardi
 * Date: 6-giu-2010
 * Time: 11.44.05
 */
@SuppressWarnings("unchecked")
public class FormTransactionData implements DirectTransactionData {

   private Map<String, Object> formData = new HashMap<String, Object>();

   public void put(String paramName, Object paramValue) {
      Object previousParam = this.formData.get(paramName);
      if(previousParam != null) {
         if(!(previousParam instanceof List)) {
            List multipleValues = new ArrayList();
            multipleValues.add(previousParam);
            this.formData.put(paramName, multipleValues);
         } else {
            ((List) previousParam).add(paramValue);
         }
      }
      this.formData.put(paramName, paramValue);
   }

   public <T> T parseValue(int order, Type type) {
      if(order == 0) {

         Class aClass = null;
         if(type instanceof ParameterizedType) {
            aClass = (Class) ((ParameterizedType) type).getRawType();
         }
         if(type instanceof Class) {
            aClass = (Class) type;
         }

         if(aClass != null && Map.class.isAssignableFrom(aClass)) {
            return (T) this.formData;
         }

         JsonParser parser = DirectContext.get().getConfiguration().getParser();
         String fromMapToJson = parser.format(this.formData);
         return (T) parser.parse(fromMapToJson, type);
      }

      throw new DirectException("Cannot access to parameter with order: " + order + " with form data");
   }

   @Override
   public <T> T parseValue(String name, Type type) {
      Object value = this.formData.get(name);
      if(value instanceof String) {
         JsonParser parser = DirectContext.get().getConfiguration().getParser();
         value = parser.parse("\"" + ((String) value).replace("\"", "\\\"") + "\"", type);
      }
      return (T) value;
   }
}
