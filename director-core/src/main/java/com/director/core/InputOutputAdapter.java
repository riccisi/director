package com.director.core;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/10/2013
 */
public interface InputOutputAdapter {

   String readProviderId();

   DirectTransaction[] readTransactions();

   void write(DirectEvent... events);

   void write(String content);
}
