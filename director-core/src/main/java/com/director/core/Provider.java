package com.director.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @author Simone Ricciardi
 * @version 1.0, 01/09/2011
 */
public interface Provider extends Serializable {

   String getUrl();

   String getId();

   String getNamespace();

   String getType();

   void registerMethod(String actionName, String methodName, Class actionClass, Method method);

   void process(InputOutputAdapter inputOutputAdapter);
}
