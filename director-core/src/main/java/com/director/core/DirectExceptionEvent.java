package com.director.core;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Author: Simone Ricciardi
 * Date: 30-mag-2010
 * Time: 13.27.31
 */
public class DirectExceptionEvent extends DirectEvent {

   private Integer tid;
   private String message;
   private String where;
   private transient Throwable exception;

   public DirectExceptionEvent(Throwable exception) {
      this(null, exception);
   }

   public DirectExceptionEvent(Integer tid, Throwable exception) {
      this.tid = tid;
      this.type = "exception";
      this.message = exception.getMessage();
      this.exception = exception;

      StringWriter sw = new StringWriter();
      this.exception.printStackTrace(new PrintWriter(sw));
      this.where = sw.toString();
   }

   public String getMessage() {
      return message;
   }

   public String getWhere() {
      return where;
   }

   public Throwable getException() {
      return exception;
   }
}
