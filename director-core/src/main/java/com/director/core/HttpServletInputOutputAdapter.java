package com.director.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * InputOutputAdapter that handles requests from HTTP protocol coming from a standard J2EE servlet environment.
 *
 * @author Simone Ricciardi
 * @version 1.0, 10/10/2013
 */
public class HttpServletInputOutputAdapter implements InputOutputAdapter {

   private static final String ENCODING = "UTF-8";

   private final HttpServletRequest request;
   private final HttpServletResponse response;
   private final RequestHandler requestHandler;

   public HttpServletInputOutputAdapter(HttpServletRequest request, HttpServletResponse response) {
      this.request = request;
      this.response = response;

      this.response.setContentType("text/plain");
      this.response.setCharacterEncoding(ENCODING);
      this.response.setHeader("Content-Disposition", "inline");

      this.requestHandler = RequestHandler.getInstance(request, response);
   }

   @Override
   public String readProviderId() {
      return request.getParameter("providerId");
   }

   @Override
   public DirectTransaction[] readTransactions() {
      return this.requestHandler.parse();
   }

   @Override
   public void write(DirectEvent... events) {
      this.requestHandler.format(events);
   }

   @Override
   public void write(String content) {
      try {
         this.response.getWriter().write(content);
      } catch(IOException e) {
         throw new DirectException(e);
      }
   }

   public HttpServletRequest getRequest() {
      return request;
   }

   public HttpServletResponse getResponse() {
      return response;
   }
}
