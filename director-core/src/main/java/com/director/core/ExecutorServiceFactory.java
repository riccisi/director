package com.director.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Simone Ricciardi
 * @version 1.0, 02/10/2014
 */
public interface ExecutorServiceFactory {

   ExecutorServiceFactory DEFAULT = new ExecutorServiceFactory() {

      @Override
      public ExecutorService create(int nThreads) {
         return Executors.newFixedThreadPool(nThreads);
      }
   };

   ExecutorService create(int nThreads);
}
