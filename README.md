# Welcome

Director provides a generic and extendible [Ext.Direct](http://www.sencha.com/products/extjs/extdirect/) implementation api that can be used stand-alone or, using the provided extensions, integrated with some of the most popular java libraries.

Currently created extensions are:

* Director for Spring
* Director for Hibernate

Planned extensions:

* Director for Axon
* Director for Atmosphere

See the [documentation](Documentation) to understand how director can be itegrated with those differents architectures.

## Ext.Direct supported features

Director supports almost all the features suggested in the Ext Direct specification, here is a short list:

* Configuration of the remoted methods by annotations.
* Automatic generation of the API.
* Marshalling/unmarshalling of parameters from and to JSON using the powerful google-gson library.
* Asynchronous method calls.
* Method batching.
* Named parameters
* File upload.

In additions Director adds some useful features (like dependency injection, AOP interceptors, Parameter factories, etc..) inherited from the framework of the choosed extension, see the [documentation](Documentation) for more detail.

## Maven

Direcor is built using Maven, so can be easily used as a dependency in your Maven project.
