package com.director.hibernate.json.impl.gson;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/15/2011
 */
public class Entity2 {

   private Long id;

   private String prop;

   public Entity2() {
   }

   public Entity2(Long id, String prop) {
      this.id = id;
      this.prop = prop;
   }

   public String getProp() {
      return this.prop;
   }
}
