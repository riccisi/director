package com.director.hibernate.json.impl.gson;

import com.director.core.json.impl.gson.GsonParser;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/15/2011
 */
public class EntitySerializerTest {

   private SessionFactory sessionFactory;

   private GsonParser gsonParser;

   @Before
   public void setup() {
      this.sessionFactory = new Configuration().configure().buildSessionFactory();

      this.gsonParser = new GsonParser();
      this.gsonParser.addConfigurer(new HibernateGsonBuilderConfigurer(this.sessionFactory));

      Entity1 entity1 = new Entity1(1L, "entity1.prop");
      entity1.setManyToOne(new Entity2(1L, "entity2.prop"));
      entity1.addOneToMany(new Entity2(2L, "entity2[0].prop"));
      entity1.addOneToMany(new Entity2(3L, "entity2[1].prop"));

      Entity3 entity4 = new Entity3(4L, "entity4.prop", "entity4.childProp");
      entity4.setManyToOne(new Entity2(7L, "entity2.prop"));
      entity4.addOneToMany(new Entity2(8L, "entity2[0].prop"));
      entity4.addOneToMany(new Entity2(9L, "entity2[1].prop"));

      Transaction transaction = this.sessionFactory.getCurrentSession().beginTransaction();
      this.sessionFactory.getCurrentSession().save(entity1);
      this.sessionFactory.getCurrentSession().save(entity4);
      transaction.commit();
      this.sessionFactory.getCurrentSession().close();
   }

   @After
   public void dispose() {
      this.sessionFactory.close();
   }

   @Test
   public void serializationTest() {

      Transaction transaction = this.sessionFactory.getCurrentSession().beginTransaction();

      Entity1 entity1 = (Entity1) this.sessionFactory.getCurrentSession().load(Entity1.class, 1L);

      String json = this.gsonParser.format(entity1);

      System.out.println("json = " + json);

      assertEquals("{\"id\":1,\"prop\":\"entity1.prop\"}", json);

      entity1.getManyToOne().getProp();

      json = this.gsonParser.format(entity1);

      System.out.println("json = " + json);

     assertEquals("{\"id\":1,\"prop\":\"entity1.prop\",\"manyToOne\":{\"id\":1,\"prop\":\"entity2.prop\"}}", json);

      entity1.getOneToMany().iterator();

      json = this.gsonParser.format(entity1);

      System.out.println("json = " + json);

     assertEquals("{\"id\":1,\"prop\":\"entity1.prop\",\"manyToOne\":{\"id\":1,\"prop\":\"entity2.prop\"},\"oneToMany\":[{\"id\":2,\"prop\":\"entity2[0].prop\"},{\"id\":3,\"prop\":\"entity2[1].prop\"}]}", json);

      transaction.commit();
      this.sessionFactory.getCurrentSession().close();
   }
}
