package com.director.hibernate.json.impl.gson;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/15/2011
 */
public class Entity1 {

   private Long id;

   private String prop;

   private Entity2 manyToOne;

   private Set<Entity2> oneToMany = new HashSet<Entity2>();

   public Entity1() {
   }

   public Entity1(Long id, String prop) {
      this.id = id;
      this.prop = prop;
   }

   public void setManyToOne(Entity2 manyToOne) {
      this.manyToOne = manyToOne;
   }

   public void addOneToMany(Entity2 oneToMany) {
      this.oneToMany.add(oneToMany);
   }

   public String getProp() {
      return prop;
   }

   public Entity2 getManyToOne() {
      return manyToOne;
   }

   public Set<Entity2> getOneToMany() {
      return oneToMany;
   }
}
