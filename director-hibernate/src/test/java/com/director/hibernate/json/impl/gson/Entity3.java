package com.director.hibernate.json.impl.gson;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/19/2011
 */
public class Entity3 extends Entity1{

   private String childProp;

   public Entity3() {
   }

   public Entity3(Long id, String prop, String childProp) {
      super(id, prop);
      this.childProp = childProp;
   }

   public Entity3(String childProp) {
      this.childProp = childProp;
   }

   public String getChildProp() {
      return childProp;
   }
}
