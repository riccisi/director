package com.director.hibernate;

import com.director.core.ParameterFactory;
import org.hibernate.SessionFactory;

import java.lang.reflect.Type;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/16/2011
 */
public class MergeParameterFactory implements ParameterFactory {

   private SessionFactory sessionFactory;

   public MergeParameterFactory(SessionFactory sessionFactory) {
      this.sessionFactory = sessionFactory;
   }

   @Override
   public Type getInputType() {
      return Object.class;
   }

   @Override
   public Object create(Type type, Object input) {
      return this.sessionFactory.getCurrentSession().merge(input);
   }
}
