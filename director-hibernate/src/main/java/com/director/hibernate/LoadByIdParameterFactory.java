package com.director.hibernate;

import com.director.core.ParameterFactory;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.lang.reflect.Type;

/**
 * @author Simone Ricciardi
 * @version 1.0, 10/16/2011
 */
public abstract class LoadByIdParameterFactory<T extends Serializable> implements ParameterFactory<T,Object> {

   private SessionFactory sessionFactory;

   public LoadByIdParameterFactory(SessionFactory sessionFactory) {
      this.sessionFactory = sessionFactory;
   }

   @Override
   public abstract Type getInputType();

   @Override
   public Object create(Type type, T id) {
      return sessionFactory.getCurrentSession().load(type.getClass(), id);
   }
}
